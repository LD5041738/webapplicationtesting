package test;

import java.io.IOException;

import org.testng.annotations.Test;

import exceptions.UserDefineException;
import pages.elementpage.CheckBoxPage;
import pages.elementpage.ElementsPage;
import pages.elementpage.TextBoxPage;
import pages.homepage.HomePage;
import reporter.ReportTestInit;
import suite.TestSuiteConfiguration;
import utils.DataObject;
import utils.ReadExcel;

public class UserStory3 extends TestSuiteConfiguration
{
static String className= UserStory3.class.getName().replace("test.","");
	
	//Validate if Element option is clickable on homepage, Check Box Option is clickble and perform operation in it. 
	@Test
	public static void TC_01() throws IOException, UserDefineException
	{
		System.out.println("<--------------------------UserStory3-------------------------------------------------------->");
		String testCaseName=new Object(){}.getClass().getEnclosingMethod().getName();
		ReadExcel.getInstance().checkRunStatus(className, testCaseName);
		ReportTestInit.getInstance().reportInit(className+"-"+testCaseName);
		
		ElementsPage.getInstance(). 
		clickCheckBoxOption();
		
		
		CheckBoxPage.getInstance().
		getPageTitle().
		expandOptions().
		selectCheckBox("Desktop").
		deselectCheckBox("Notes").
		collapseOptions();
	}
}

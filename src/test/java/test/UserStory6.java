package test;

import java.io.IOException;

import org.testng.annotations.Test;

import exceptions.UserDefineException;
import pages.elementpage.ActionsButtonPage;
import pages.elementpage.ElementsPage;
import pages.elementpage.WebTablePage;
import reporter.ReportTestInit;
import suite.TestSuiteConfiguration;
import utils.DataObject;
import utils.ReadExcel;

public class UserStory6 extends TestSuiteConfiguration
{
	static String className= UserStory6.class.getName().replace("test.","");
	 
	@Test
	public static void TC_01() throws IOException, UserDefineException
	{
		System.out.println("<--------------------------UserStory6-------------------------------------------------------->");
		String testCaseName=new Object(){}.getClass().getEnclosingMethod().getName();
		ReadExcel.getInstance().checkRunStatus(className, testCaseName);
		ReportTestInit.getInstance().reportInit(className+"-"+testCaseName);
		
		ElementsPage.getInstance(). 
		clickButtonsOption();
		
		ActionsButtonPage.getInstance().
		getPageTitle().
		clickDoubleClickButton().
		clickRightClickButton().
		clicksimpleClickButton().
		validation();
	}
}

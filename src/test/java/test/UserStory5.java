package test;

import java.io.IOException;

import org.testng.annotations.Test;

import exceptions.UserDefineException;
import pages.elementpage.ElementsPage;
import pages.elementpage.RadioButtonPage;
import pages.elementpage.WebTablePage;
import reporter.ReportTestInit;
import suite.TestSuiteConfiguration;
import utils.DataObject;
import utils.ReadExcel;

public class UserStory5 extends TestSuiteConfiguration
{
	static String className= UserStory5.class.getName().replace("test.","");
	 
	@Test
	public static void TC_01() throws IOException, UserDefineException
	{
		System.out.println("<--------------------------UserStory5-------------------------------------------------------->");
		String testCaseName=new Object(){}.getClass().getEnclosingMethod().getName();
		ReadExcel.getInstance().checkRunStatus(className, testCaseName);
		ReportTestInit.getInstance().reportInit(className+"-"+testCaseName);
		
		ElementsPage.getInstance(). 
		clickWebTableOption();
		
		WebTablePage.getInstance().
		getPageTitle().
		addValueIntoWebTable().
		setFirstNameInRegestrationForm(DataObject.getVariable("FName", testCaseName)).
		setLastNameInRegestrationForm(DataObject.getVariable("LName", testCaseName)).
		setEmailInRegestrationForm(DataObject.getVariable("Email", testCaseName)).
		setAgeInRegestrationForm(DataObject.getVariable("Age", testCaseName)).
		setSalaryInRegestrationForm(DataObject.getVariable("Salary", testCaseName)).
		setDepartmentInRegestrationForm(DataObject.getVariable("Dept", testCaseName)).
		clickSubmitButton().
		searchOperation(DataObject.getVariable("Search", testCaseName)).
		validateInputs();
	}
}

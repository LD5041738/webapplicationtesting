package elements;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import driver.BasePage;

public class RadioButtonElement extends CommonElements
{
	private static Logger logger = Logger.getLogger(RadioButtonElement.class);
	String radioButtonName;
	
	public RadioButtonElement(WebElement element, String elementName) 
	{
		super(element, elementName);
	}
	
	public boolean isAlreadySelected()
	{
		int alreadySelected=BasePage.driver.findElements(By.xpath("//span[text()='Impressive' or text()='Yes']")).size();
		if(alreadySelected>0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public void select()
	{
		radioButtonName=getName();
		if(isAlreadySelected())
		{
			logger.info("Radio button is already selected");
			Assert.assertTrue(false);
		}
		else
		{
			getWrappedElement().click();
			logger.info("Radio button selected");
		}
	}
}

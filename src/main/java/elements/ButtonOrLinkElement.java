package elements;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;

public class ButtonOrLinkElement extends CommonElements
{
	private static Logger logger = Logger.getLogger(ActionsClass.class);
	public ButtonOrLinkElement(WebElement element, String elementName)
	{
		super(element, elementName);
	}
	
	@Override
	public void click() 
	{
		System.out.println(String.format("click on ['%s'] button",getName()));
		getWrappedElement().click();
		logger.info(String.format("click on ['%s'] button",getName()));
	}
	
	@Override
	public void submit() 
	{
		System.out.println(String.format("click on ['%s'] button",getName()));
		if(getName().contains("submit"))
		getWrappedElement().submit();
		logger.info(String.format("Submit on ['%s'] button",getName()));
	}
}

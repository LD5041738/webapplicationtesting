package pages.elementpage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import driver.BasePage;
import elements.RadioButtonElement;
import exceptions.UserDefineException;
import reporter.ExtendReport;
import waits.Wait;

public class RadioButtonPage extends BasePage
{
	@FindBy(xpath = "//div[text()='Radio Button']") WebElement pageTitle;
	
	ExtendReport ex= new ExtendReport();
	
	private RadioButtonPage()
	{
		PageFactory.initElements(driver,this);
	}
	
	public static RadioButtonPage getInstance()
	{
		return new RadioButtonPage();
	}
	
	//---------------------------------------Validation on page ------------------------------------------------
	
	public RadioButtonPage getPageTitle() throws UserDefineException
	{
		Wait.getInstance().waitUntilElementVisible(pageTitle);
		if(pageTitle.getText().equals("Radio Button"))
		{
			System.out.println("Element is visible");
			ex.login.log(Status.PASS, "Title Validated");
		}
		else
		{
			ex.login.log(Status.FAIL, "Title Validation fail");
			throw new UserDefineException("Element not visible");
		}
		return this;
	}
	
	//------------------------------------------Operations on page ------------------------------------------------
	
	public RadioButtonPage selectOption(String value)
	{
		String actualvalue=value;
		String expectedValue=actualvalue.toLowerCase()+"Radio";
		WebElement element= driver.findElement(By.xpath("//label[@for='"+expectedValue+"']"));
		new RadioButtonElement(element, expectedValue).select();
		ex.login.log(Status.PASS, actualvalue+" radio button clicked");
		return this;
	}
}

package pages.elementpage;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import driver.BasePage;
import elements.ActionsClass;
import exceptions.UserDefineException;
import reporter.ExtendReport;
import waits.Wait;

public class ActionsButtonPage extends BasePage
{
	@FindBy(xpath = "//div[text()='Buttons']") WebElement pageTitle;
	@FindBy(xpath = "//button[@id='doubleClickBtn']") WebElement doubleClickButton;
	@FindBy(xpath = "//button[@id='rightClickBtn']") WebElement rightClickButton;
	@FindBy(xpath = "//button[text()='Click Me']") WebElement clickMeButton;
	
	@FindBy(xpath = "//p[@id='doubleClickMessage']") WebElement postValiadtionDoubleClick;
	@FindBy(xpath = "//p[@id='rightClickMessage']") WebElement postValiadtionRightClick;
	@FindBy(xpath = "//p[@id='dynamicClickMessage']") WebElement postValiadtionSimpleClick;
	
	ExtendReport ex= new ExtendReport();
	String actionPerformed="";
	
	private ActionsButtonPage()
	{
		PageFactory.initElements(driver,this);
	}
	
	public static ActionsButtonPage getInstance()
	{
		return new ActionsButtonPage();
	}
	
	//---------------------------------------Validation on page ------------------------------------------------
	
	public ActionsButtonPage getPageTitle() throws UserDefineException
	{
		Wait.getInstance().waitUntilElementVisible(pageTitle);
		if(pageTitle.getText().equals("Buttons"))
		{
			System.out.println("Element is visible");
			ex.login.log(Status.PASS, "Title Validated");
		}
		else
		{
			ex.login.log(Status.FAIL, "Title Validation fail");
			throw new UserDefineException("Element not visible");
		}
		return this;
	}
	
	//------------------------------------------Operations on page ------------------------------------------------
	
	public ActionsButtonPage clickDoubleClickButton()
	{
		actionPerformed+="DoubleClick";
		new ActionsClass(doubleClickButton, "DoubleClick").doubleClick();
		ex.login.log(Status.PASS, "Performed 'Double' click");
		return this;
	}
	
	public ActionsButtonPage clickRightClickButton()
	{
		actionPerformed+=" RightClick";
		new ActionsClass(rightClickButton, "RightClick").rightClick();
		ex.login.log(Status.PASS, "Performed 'Right' click");
		return this;
	}
	
	public ActionsButtonPage clicksimpleClickButton()
	{
		actionPerformed+=" ClickMe";
		new ActionsClass(clickMeButton, "ClickMe").simpleClick();
		ex.login.log(Status.PASS, "Performed 'Simple' click");
		return this;
	}
	//------------------------------------------Post operation Validation -------------------------------------------
	
	public ActionsButtonPage validation()
	{
		String arr[]=actionPerformed.split("\\s");
		int operationsPerformed=arr.length;
		for(int i=0;i<=operationsPerformed-1;i++)
		{
			if(arr[i].equals("DoubleClick"))
			{
				ex.login.log(Status.PASS, "You have done a double click : Validation done");
				Assert.assertEquals(postValiadtionDoubleClick.getText(),"You have done a double click");
			}
			else if(arr[i].equals("RightClick"))
			{
				ex.login.log(Status.PASS, "You have done a right click : Validation done");
				Assert.assertEquals(postValiadtionRightClick.getText(),"You have done a right click");
			}
			else if(arr[i].equals("ClickMe"))
			{
				ex.login.log(Status.PASS, "You have done a dynamic click : Validation done");
				Assert.assertEquals(postValiadtionSimpleClick.getText(),"You have done a dynamic click");
			}
		}
		
		return this;
	}
}

package listeners;

import org.testng.IExecutionListener;

public class IExecutionListnerClass implements IExecutionListener 
{
	//This class will start execution before any annotation in testng.
	long startTime;
	long endTime;
	@Override
	public void onExecutionStart() 
	{
		startTime= System.currentTimeMillis();
		System.out.println("**************************Execution of test Suite Started at"+startTime+"*************************************");
	}

	@Override
	public void onExecutionFinish() 
	{
		endTime= System.currentTimeMillis();
		System.out.println("**************************Execution of test Suite Ends at"+endTime+"*************************************");
		System.out.println("**************************Time taken to execuite Suite :"+(endTime-startTime)+"**************************");
	}

}

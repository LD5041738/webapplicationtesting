package listeners;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

//This class is used to rerun the fail TC
public class IRetryAnalyzerClass implements IRetryAnalyzer 
{
	static int count=0;
	static int maxTry=3;
	@Override
	public boolean retry(ITestResult result) 
	{
		
		if(!result.isSuccess())
		{
			if(count<maxTry)
			{
				count++;
				result.setStatus(result.FAILURE);
				return true;
			}
			else
			{
				result.setStatus(result.FAILURE);
			}
		}
		else
		{
			result.setStatus(result.SUCCESS);
		}
		return false;
	}

}

package suite;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import driver.DriverClass;

public class TestSuiteConfiguration 
{
	static WebDriver driver;
	
	@BeforeSuite
	public static void beforeSuite()
	{
		DOMConfigurator.configure("log4j.xml");
	}
	
	@BeforeTest
	public static void beforeTest()
	{
		driver=DriverClass.driverSetup();
	}
	
	@BeforeClass
	public static void beforeClass()
	{
		
	}
	
	@BeforeMethod
	public static void beforeMethod()
	{
		
	}
	
	@AfterMethod
	public static void afterMethod()
	{
		
	}
	
	@AfterClass
	public static void afterClass()
	{
		
	}
	
	@AfterTest
	public static void afterTest()
	{
		
	}
	
	@AfterSuite
	public static void afterSuite()
	{
		driver.quit();
	}
}

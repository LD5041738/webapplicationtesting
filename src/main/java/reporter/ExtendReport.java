package reporter;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

import utils.Constants;

public class ExtendReport 
{
	public static ExtentHtmlReporter htmlReporter;
	public static ExtentReports extent;
	public static ExtentTest login;
	public static String ReportName;
	
	public static ExtendReport getInstance()
	{
		return new ExtendReport();
	}
	
	public String getDate()
	{
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MMM,dd,yyyy");
		LocalDate sysDate = LocalDate.now();
		String formattedDate = dtf.format(sysDate);
		String date = formattedDate;
		return date;
	}
	
	public String startReport() throws UnknownHostException
	{
		String date = getDate();
		htmlReporter = new ExtentHtmlReporter(Constants.reportPath+"/"+date+"WebApplicationTesting.html");
		extent = new ExtentReports ();
		extent.attachReporter(htmlReporter);
		extent.setSystemInfo("Host Name", InetAddress.getLocalHost().getHostName());
	    htmlReporter.config().setDocumentTitle(Constants.documentName);
	    htmlReporter.config().setReportName(Constants.reportName);
	    htmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
	    htmlReporter.config().setChartVisibilityOnOpen(true);
	    htmlReporter.config().setTheme(Theme.DARK);
	    htmlReporter.config().setEncoding("utf-8");
	    htmlReporter.config().setCSS("css-string");
		htmlReporter.config().setJS("js-string");
		ReportName =Constants.reportPath+"/"+date+"WebApplicationTesting.html";
		return ReportName;
	}
	
	public void endReport()
	{
		 extent.flush();
	}
}

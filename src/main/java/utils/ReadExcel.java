package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import org.apache.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.SkipException;

public class ReadExcel
{
	private static Logger logger = Logger.getLogger(ReadExcel.class);
	public static ReadExcel getInstance()
	{
		return new ReadExcel();
	}
	
	public void readTestDataSheet(String tabName) throws IOException
	{
		try
		{
		DataObject obj=new DataObject();
		HashMap<String,LinkedHashMap<String, String>> excel=new HashMap<String, LinkedHashMap<String,String>>();
		String tabName1=tabName;
		String filePath= Constants.testDataPath;
		File file=new File(filePath);
		FileInputStream fis=new FileInputStream(file);
		String filePathValidation=filePath;
		if(filePathValidation.toString().endsWith(".xlsx"))
		{
			XSSFWorkbook workbook =new XSSFWorkbook(fis);
			XSSFSheet sheet=workbook.getSheet(tabName1);
			int rowCount=sheet.getPhysicalNumberOfRows();
			for(int i=1;i<rowCount;i++)
			{
				LinkedHashMap<String,String> list=new LinkedHashMap<String,String>();
				int columnCount=sheet.getRow(i).getLastCellNum();
				for(int j=0;j<columnCount;j++)
				{
					String columnName= sheet.getRow(0).getCell(j).toString();
					String columnValue=sheet.getRow(i).getCell(j).toString();
					if(columnName.equalsIgnoreCase("TC_ID"))
					{
						obj.setKey(columnValue);
					}
					list.put(columnName, columnValue);
				}
				excel.put(obj.getKey(),list);
			}
			obj.setExcelObject(excel);
			fis.close();
			logger.info("Test Data loaded");
		}
		}
		catch(Exception e)
		{
			logger.warn("Test Data File doesn't get loaded");
			throw new SkipException(String.format("Read Test Data Fails"));
		}
	}
	
	public boolean checkRunStatus(String className,String testCaseName) throws IOException
	{
		readTestDataSheet(className);
		String decision=DataObject.getVariable("Run",testCaseName);
		if(decision.equalsIgnoreCase("Yes"))
		{
			logger.info("Run Status is YES");
			return true;
		}
		else
		{
			logger.warn("Run Status is NO");
			return false;
		}
		
	}
}


